import React from "react";
import {
  View,
  TextInput,
  Dimensions,
  Platform,
  Text,
  FlatList
} from "react-native";
import { Icon } from "react-native-elements";

const { width } = Dimensions.get("window");
const { height } = Dimensions.get("window");

const dummyData = [
  {
    message: "Hai, Apakah produk ini masih ada?"
  },
  {
    message: "Bisa dikirim hari ini?"
  },
  {
    message: "Terima kasih"
  }
];

const MessageInput = props => {
  return (
    <View>
      <View
        style={[
          styles.containerStyle,
          { borderWidth: 1.5, borderColor: "#e4e4e4" }
        ]}
      >
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={dummyData}
          keyExtractor={(item, index) => item.message + ""}
          renderItem={({ item, index }) => (
            <View
              style={{
                height: 28,
                backgroundColor: "white",
                borderRadius: 18,
                borderColor: "red",
                borderWidth: 1,
                padding: 2,
                paddingHorizontal: 4,
                margin: 4
              }}
            >
              <Text style={{ textAlign: "center", color: "#e06860" }}>
                {item.message}
              </Text>
            </View>
          )}
        />
      </View>

      <View style={styles.containerStyle}>
        <Icon
          containerStyle={{ marginLeft: 10 }}
          iconStyle={{ margin: 0, padding: 0 }}
          name="plus"
          type="font-awesome"
          color={"#494e57"}
          size={20}
          onPress={props.onLeftPress}
        />
        <View style={styles.inputViewStyle}>
          <TextInput
            style={{
              color: "#212529",
              ...Platform.select({
                android: {
                  minHeight: 36,
                  width: width - 76
                },
                ios: {
                  minHeight: 36,
                  width: width - 76
                }
              })
            }}
            placeholder={"Ketik pesan kamu..."}
            autoCapitalize="none"
            autoCorrect={false}
            selectionColor={"#212529"}
            underlineColorAndroid="transparent"
            value={props.textMessage}
            onChangeText={props.onChangeText}
          />
        </View>
        <Icon
          containerStyle={{ marginLeft: 0, marginRight: 10 }}
          iconStyle={{ margin: 0, padding: 0 }}
          name="envelope"
          type="font-awesome"
          color={props.textMessage.length > 0 ? "#7d62d9" : "#494e57"}
          size={20}
          onPress={props.onRightPress}
        />
      </View>
    </View>
  );
};

const styles = {
  containerStyle: {
    flexDirection: "row",
    backgroundColor: "#fff"
  },
  inputViewStyle: {
    width: "78%",
    height: 50,
    borderWidth: 1,
    borderColor: "#c8c8c8",
    paddingLeft: 8,
    paddingRight: 8,
    borderRadius: 8,
    margin: 12
  },
  inputStyle: {
    fontSize: 13,
    backgroundColor: "#fff"
  }
};

export { MessageInput };
