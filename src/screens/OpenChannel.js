import React, { Component } from "react";
import {
  View,
  FlatList,
  TouchableHighlight,
  Text,
  Image,
  TextInput
} from "react-native";
import { connect } from "react-redux";
import {
  initOpenChannel,
  getOpenChannelList,
  onOpenChannelPress,
  clearCreatedOpenChannel,
  clearSeletedOpenChannel,
  openChannelProgress
} from "../actions";
import { Button, ListItem, Avatar, Spinner } from "../components";
import { sbCreateOpenChannelListQuery } from "../sendbirdActions";
import appStateChangeHandler from "../appStateChangeHandler";

class OpenChannel extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    return {
      title: "Chat",
      headerTitleStyle: {
        color: "#7E7E7E",
        fontSize: 16,
        textAlign: "center",
        alignSelf: "center"
      },
      headerLeft: (
        <Button
          containerViewStyle={{ marginLeft: 0, marginRight: 0 }}
          buttonStyle={{ paddingLeft: 14 }}
          icon={{
            name: "chevron-left",
            type: "font-awesome",
            color: "#C5C5C5",
            size: 18
          }}
          backgroundColor="transparent"
          onPress={() => navigation.goBack()}
        />
      ),
      headerRight: (
        <Text
          style={{ paddingRight: 14, color: "#7E7E7E" }}
          onPress={() => {
            navigation.navigate("OpenChannelCreate");
          }}
        >
          Pilih
        </Text>
      )
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      enterChannel: false,
      openChannelListQuery: null,
      list: []
    };
  }

  componentDidMount() {
    this.willFocusSubsription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this._initOpenChannelList();
      }
    );
    this.appStateHandler = appStateChangeHandler
      .getInstance()
      .addCallback("OPEN_CHANNEL", () => {
        this._initOpenChannelList();
      });
  }

  componentWillUnmount() {
    this.appStateHandler();
    this.willFocusSubsription.remove();
  }

  componentWillReceiveProps(props) {
    const { list, channel, createdChannel } = props;
    if (createdChannel) {
      const newList = [...[createdChannel], ...list];
      this.setState({ list: newList }, () => {
        this.props.clearCreatedOpenChannel();
      });
    }
    if (channel) {
      this.props.clearSeletedOpenChannel();
      this.props.navigation.navigate("Chat", {
        channelUrl: channel.url,
        title: channel.name,
        memberCount: channel.participantCount,
        isOpenChannel: channel.isOpenChannel(),
        _initListState: this._initEnterState
      });
    }
  }

  _initEnterState = () => {
    this.setState({ enterChannel: false });
  };

  _initOpenChannelList = () => {
    this.props.initOpenChannel();
    this._getOpenChannelList(true);
  };

  _getOpenChannelList = init => {
    this.props.openChannelProgress(true);
    if (init) {
      const openChannelListQuery = sbCreateOpenChannelListQuery();
      this.setState({ openChannelListQuery }, () => {
        this.props.getOpenChannelList(this.state.openChannelListQuery);
      });
    } else {
      this.props.getOpenChannelList(this.state.openChannelListQuery);
    }
  };

  _onListItemPress = channelUrl => {
    if (!this.state.enterChannel) {
      this.setState({ enterChannel: true }, () => {
        this.props.onOpenChannelPress(channelUrl);
      });
    }
  };

  // _handleScroll = (e) => {
  //     if (e.nativeEvent.contentOffset.y < -100 && !this.props.isLoading) {
  //         this._initOpenChannelList();
  //     }
  // }

  _renderList = rowData => {
    const channel = rowData.item;
    return (
      <ListItem
        component={TouchableHighlight}
        hideChevron
        containerStyle={{ backgroundColor: "#fff" }}
        key={channel.url}
        avatar={
          <Avatar
            rounded
            source={
              channel.coverUrl
                ? { uri: channel.coverUrl }
                : require("../img/icon_sb_68.png")
            }
          />
        }
        title={
          channel.name.length > 30
            ? channel.name.substring(0, 26) + "..."
            : channel.name
        }
        titleStyle={{ fontWeight: "500", fontSize: 16 }}
        subtitle={"Produk Jam D-Zinner Tipe Garis Putih masih ada"}
        subtitleStyle={{ fontWeight: "300", fontSize: 12 }}
        onPress={() => this._onListItemPress(channel.url)}
      />
    );
  };

  render() {
    return (
      <View>
        <Spinner visible={this.props.isLoading} />
        <View
          style={{
            margin: 14,
            backgroundColor: "#EBEBEB",
            borderRadius: 4,
            flexDirection: "row",
            paddingHorizontal: 6
          }}
        >
          <Image
            style={{
              width: 32,
              height: 32,
              alignSelf: "center",
              marginRight: 4
            }}
            source={require("../img/ic_search.png")}
          />
          <TextInput
            placeholder="Cari chat"
            maxLength={46}
            underlineColorAndroid="transparent"
          />
        </View>
        <View style={{ height: 2, backgroundColor: "#E3E3E3" }} />
        <FlatList
          renderItem={this._renderList}
          data={this.props.list}
          showsVerticalScrollIndicator={false}
          extraData={this.state}
          inverted={false}
          keyExtractor={(item, index) => item.url}
          onEndReached={() => this._getOpenChannelList(false)}
          onEndReachedThreshold={0}
        />
      </View>
    );
  }
}

const styles = {};

function mapStateToProps({ openChannel }) {
  const { isLoading, list, createdChannel, channel } = openChannel;
  return { isLoading, list, createdChannel, channel };
}

export default connect(
  mapStateToProps,
  {
    initOpenChannel,
    getOpenChannelList,
    onOpenChannelPress,
    clearCreatedOpenChannel,
    clearSeletedOpenChannel,
    openChannelProgress
  }
)(OpenChannel);
