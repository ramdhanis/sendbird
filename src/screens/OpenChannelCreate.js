import React, { Component } from "react";
import { View, Text, Image, TextInput, Platform } from "react-native";
import { connect } from "react-redux";
import {
  createOpenChannel,
  initOpenChannelCreate,
  addOpenChannelItem,
  openChannelProgress,
  onOpenChannelPress
} from "../actions";
import { Button, Input, Spinner, FormValidationMessage } from "../components";
import SendBird from "sendbird";
import SelectMultiple from "../components/Multibox/SelectMultiple";

const dummyData = [
  { label: "Apples", value: "appls" },
  { label: "Oranges", value: "orngs" },
  { label: "Pears", value: "pears" }
];

const renderLabel = (label, style, selected, price) => {
  return (
    <View style={{ flexDirection: "row", alignItems: "center" }}>
      <Image
        style={{
          width: 42,
          height: 42,
          borderRadius: 4,
          backgroundColor: "white"
        }}
        source={require("../img/dummy_watch.png")}
      />
      <View style={{ marginLeft: 10 }}>
        <Text style={style}>{label}</Text>
        <Text style={{ color: "#DA3A28", fontWeight: "bold", marginTop: 4 }}>
          {price}
        </Text>
      </View>
    </View>
  );
};

class OpenChannelCreate extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    return {
      title: "Produk",
      headerTitleStyle: {
        color: "#DA3A28",
        fontSize: 16
      },
      headerLeft: (
        <Button
          containerViewStyle={{ marginLeft: 0, marginRight: 0 }}
          buttonStyle={{ paddingLeft: 14 }}
          icon={{
            name: "chevron-left",
            type: "font-awesome",
            color: "#C5C5C5",
            size: 18
          }}
          backgroundColor="transparent"
          onPress={() => navigation.goBack()}
        />
      )
    };
  };
  state = { selectedFruits: [] };
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      channelName: ""
    };
  }

  onSelectionsChange = selectedFruits => {
    // selectedFruits is array of { label, value }
    this.setState({ selectedFruits });
  };

  componentDidMount() {
    this.props.navigation.setParams({
      handleHeaderRight: this._onCreateButtonPress
    });
    this.props.initOpenChannelCreate();
  }

  componentWillReceiveProps(props) {
    const { channel, error } = props;

    if (channel) {
      this.props.openChannelProgress(true);
      this.setState({ isLoading: false }, () => {
        this.props.addOpenChannelItem(channel);
        this.props.navigation.goBack();
        this.props.onOpenChannelPress(channel.url);
      });
    }

    if (error) {
      this.setState({ isLoading: false });
    }
  }

  _onOpenChannelNameChanged = channelName => {
    this.setState({ channelName });
  };

  _onCreateButtonPress = () => {
    const { channelName } = this.state;
    this.setState({ isLoading: true }, () => {
      this.props.createOpenChannel(channelName);
    });
  };

  render() {
    return (
      <View style={{ backgroundColor: "#fff", flex: 1 }}>
        <Spinner visible={this.state.isLoading} />
        <View
          style={{
            margin: 14,
            backgroundColor: "#EBEBEB",
            borderRadius: 4,
            flexDirection: "row",
            paddingHorizontal: 6
          }}
        >
          <Image
            style={{
              width: 32,
              height: 32,
              alignSelf: "center",
              marginRight: 4
            }}
            source={require("../img/ic_search.png")}
          />
          <TextInput
            placeholder="Cari Berdasarkan invoice"
            maxLength={46}
            underlineColorAndroid="transparent"
          />
        </View>

        <View style={{ height: 2, backgroundColor: "#E3E3E3" }} />
        <SelectMultiple
          items={dummyData}
          renderLabel={renderLabel}
          selectedItems={this.state.selectedFruits}
          onSelectionsChange={this.onSelectionsChange}
        />
        <View
          style={{
            alignItems: "center",
            paddingVertical: 14,
            shadowOpacity: 10,
            shadowRadius: 10,
            shadowColor: "red",
            elevation: 25,
            shadowOffset: { height: 10, width: 10 }
          }}
        >
          <View
            style={{
              backgroundColor: "#DA3A28",
              borderRadius: 20,
              width: "80%"
            }}
          >
            <Text
              style={{
                fontWeight: "bold",
                color: "white",
                fontSize: 16,
                textAlign: "center",
                paddingHorizontal: 12,
                paddingVertical: 12
              }}
            >
              Kirim (2/5)
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

function mapStateToProps({ openChannelCreate }) {
  const { error, channel } = openChannelCreate;
  return { error, channel };
}

export default connect(
  mapStateToProps,
  {
    createOpenChannel,
    initOpenChannelCreate,
    addOpenChannelItem,
    openChannelProgress,
    onOpenChannelPress
  }
)(OpenChannelCreate);
